$(document).ready(function () {
	//$('.chat').trigger('click');
	//$(".chat").click( function(){
		if(getCookie("SESSION_CHAT")!=null && getCookie("SESSION_CHAT")!=""){
			
		}else{
			if(!$("#chat_login").hasClass("chat_login")){
				ChatLogin();
			}
		}
	//})

	if(getCookie("SESSION_CHAT")!=null && getCookie("SESSION_CHAT")!=""){
		loadJS("variables",BASE_URL+"/chat/chat/chatRoom.js");
		//console.log(getCookie("ID_DIAGRAMA"));
		EnviarDiagrama(getCookie("ID_DIAGRAMA"));
	}


	$("body").on("keydown",".acceso", function(e){
		if(e.keyCode === 13){ 
			EnviarForm();
		    e.preventDefault();  
		}
	})

	$("body").on("click","#btn_logeo", function(){
		if($("#chk_condicion").is(":checked")){
			EnviarForm();
		}else{
			alert("Debe aceptar los terminos y condiciones");
			return false;
		}
	})

	$("body").on("click",".criterio", function(){
		var id = $(this).attr("rel");

		EnviarDiagrama(id);
		
		var expiration = new Date();
                    expiration.setTime(expiration.getTime() + 3600000); //Caduca 1 hora
                    setCookie("ID_DIAGRAMA",id,expiration);
	})
})



function getCookie(name){
	var cname = name + "=";
	var dc = document.cookie;
	if (dc.length > 0) {
		begin = dc.indexOf(cname);
		if (begin != -1) {
			begin += cname.length;
			end = dc.indexOf(";", begin);
			if (end == -1) end = dc.length;
				return unescape(dc.substring(begin, end));
		}
	}

	return null;
}

function setCookie(name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + escape(value) +
	((expires == null) ? "" : "; expires=" + expires.toGMTString()) +
	((path == null) ? "" : "; path=" + path) +
	((domain == null) ? "" : "; domain=" + domain) +
	((secure == null) ? "" : "; secure");
}

function delCookie (name,path,domain) {
	if (getCookie(name)) {
		document.cookie = name + "=" +
		((path == null) ? "" : "; path=" + path) +
		((domain == null) ? "" : "; domain=" + domain) +
		"; expires=Thu, 01-Jan-70 00:00:01 GMT";
	}
}


function EnviarDiagrama(id)
{
	$.ajax({
      url: BASE_URL+"/chat/chat/externalClientChatRoom/"+id,
      type: "GET",
      xhrFields: {
      	withCredentials: true
      },
      crossDomain: true
    }).done( function(res){

    	console.log("ME :"+res.username);
    	console.log("ROOM :"+res.sesion);
    	console.log("UUID :"+res.uuid);
    	console.log("DATEINI :"+res.dateini);



        var ME      = res.username;
		var ROOM    = res.sesion;
		var uuid    = res.uuid;
		var DATEINI = res.dateini;
		
		if(res.username!=""){
			$("#chat_login").remove();

			var exp = new Date();
			exp.setTime(exp.getTime() + 3600000); //Caduca 1 hora
			setCookie("ID_SESSION",ROOM,exp);

			CargarChat(ME,ROOM,uuid,DATEINI);
		}else{
			CargarChat(ME,ROOM,uuid,DATEINI);
			$(".ui-chatbox-log").html('<div class="ui-chatbox-msg interno" style="display: block; max-width: 340px;"><b>Agente No Conectado</b><span>Hola, En estos momentos, no hay un agente conectado.<br/><br/>Gracias.</span></div>');
		}

		

    }).fail(function(jqXHR, textStatus, errorThrown){

    	console.log('Hubo un error al levantar el chat: '+errorThrown);
    	
    });

}


function ChatLogin()
{
	var html  = '<div class="ui-chatbox chat_login" style="background:#fff" id="chat_login">';
		html += '<div class="ui-chatbox-titlebar"><span>Chatea con un representante </span><a href="javascript:;" class="ui-corner-all ui-chatbox-icon cerrar_chat" role="button"><span class="ui-icon ui-icon-closethick ui-button-bg-images">Cerrar</span></a><a href="javascript:;" class="minimizar_chat ui-corner-all ui-chatbox-icon" role="button"><span class="ui-icon ui-icon-minusthick ui-button-bg-images">Minimizar</span></a></div>';
		html += '<div class="ui-chatbox-content" style="background:#fff">';
		html += '<div class="body_chat"><div class="block"><input type="text" name="tx_usuario" id="tx_usuario" class="acceso" placeholder="Ingrese su E-mail" /></div>';
		//html += '<div class="block"><input type="password" name="tx_password" id="tx_password" class="acceso" placeholder="Ingrese su password" /></div>';
		html += '<div class="block" style="font-size:12px;"><input type="checkbox" name="chk_condicion" id="chk_condicion" style="width:15px; height:15px;" /> Otorgo mi consentimiento para el uso de mi correo electrónico. <a href="http://ceprepuc.pucp.edu.pe/terminos-y-condiciones" target="_blank">Condiciones de uso</a><input type="button" name="btn_logeo" value="Ingresar" id="btn_logeo" /></div></div>';
		html += '<div id="img_loader_chat"><span>verificando datos</span></div>';
		html += '<div id="img_error"><span>los datos ingresados son incorrectos</span></div>';
		html += '<div id="list_diagramas"></div>';
		html += '</div>';
	    html += '</div>';

	$("body").append(html);
}

function GenerarRandon()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 25; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function EnviarForm()
{
	$("#img_loader_chat").show();
	
	setTimeout( function(){
		
		$.ajax({
        	url: BASE_URL+"/loginGET",
          	type: "GET",
          	//data: {campo1:$("#tx_usuario").val(),campo2:$("#tx_password").val()},
        	data: {campo1:$("#tx_usuario").val(),campo2:''},  
		dataType: "json",
          	xhrFields: {
          		withCredentials: true
          	},
        	crossDomain: true
        }).done( function(res){

        	$("#img_loader_chat").hide();

            if(res!=null){

            	var expiration = new Date();
		    expiration.setTime(expiration.getTime() + 3600000); //Caduca 1 hora
		    setCookie("SESSION_CHAT",GenerarRandon(),expiration);
            	
            	$.ajax({
	              url: BASE_URL+"/regla/categoria/listar",
	              type: "GET",
	              xhrFields: {
	              	withCredentials: true
	              },
	              crossDomain: true
	            }).done( function(r){

	                if(r!=null){
	                	var html = '<h2>Seleccione un criterio para empezar el chat</h2>';
	                	for(var i = 0; i < r.length; i++){
	                		html += '<span class="criterio" rel="'+r[i].iddiagrama+'">'+r[i].nombrediagrama+'</span>';
	                	}
				$(".body_chat").hide();
	                	$("#list_diagramas").html(html).show();
	                }

	                loadJS("variables",BASE_URL+"/chat/chat/chatRoom.js");

	            }).fail(function(jqXHR, textStatus, errorThrown){
	     
	            	console.log('Hubo un error al listar las reglas: '+errorThrown);
	            	
	            });

            }else{
            	$("#img_error").slideDown();
            	setTimeout(function(){
            		$("#img_error").slideUp("slow", function(){
            			$("#tx_usuario").val('');
            			//$("#tx_password").val('');
            			$("#tx_usuario").focus();
            		});
            	},3000);
            }
            

        }).fail(function(jqXHR, textStatus, errorThrown){
        	$("#img_loader_chat").hide();
        	console.log('Hubo un error al liniciar sesion: '+errorThrown);
        	
        });

	},1500);

}



function loadJS(id, src) {
    if (document.getElementById(id) != null) return;
    var js = document.createElement('script'); 
    js.id = id; 
    js.async = false; 
    js.type = "text/javascript";
    js.src = src;
    document.getElementsByTagName('head')[0].appendChild(js);
}

