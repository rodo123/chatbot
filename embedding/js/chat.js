var sesiones;
var counter;
var idList;
var wsList;
var broadcastMessageCallback;
var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket;
var activo = 0;
var inactivo = 1;


var BASE_URL = "http://ceprepucp.exper-ti.com:9000";

function CargarChat(ME,ROOM,uuid,DATEINI)
{

    var buscar = rutaChatWS;
    var n = buscar.indexOf("chatWS?idSesion=");

    
    if(n>0){

    }else{

        rutaChatWS += "chatWS?idSesion=";
    
    }
    

    console.log("rutaChatWS: <" + rutaChatWS + ">");
    
    console.log("URL=== "+buscar);
    initChatUI();
    showChat(ME,ROOM,uuid,DATEINI);
}

function initChatUI() {
	counter = 0;
    idList = new Array();
	wsList = new Array();
}

function showChat(ME,ROOM,uuid,DATEINI) {
	console.log("clic <" + ROOM + ">");
	var id = ROOM;
	var chatSocket;

	var receiveEvent = function(event) {
		var data = JSON.parse(event.data);

		if (typeof(data.error) === 'undefined') {
			
				$("#" + ROOM ).clientChatbox("option", "boxManager").addMsg(data);
		} else {
			console.log("Error: <" + data.error + ">");
			
		}
	};

	chatSocket = new WS(rutaChatWS+ROOM+"&uuid="+uuid);
	chatSocket.onmessage = receiveEvent;
	counter++;

	idList.push(id);

    chatboxManager.addBox(id, {
        dest: "dest" + counter, 
        title: ME,
        first_name: ME,
        dateIni: DATEINI,
        last_name: "",
		ws: chatSocket
        
    });
}


function toHHMMSS ( strsec ) {
    var sec_num = parseInt(strsec, 10); 
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
}

function checkTime(i) {
    if (i<10) {i = "0" + i}; 
    return i;
}

function getHHMMSS( longTime ) {
    var date = new Date(longTime);
    var h=date.getHours();
    var m=date.getMinutes();
    var s=date.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    return h + ":" + m + ":" + s;
}
