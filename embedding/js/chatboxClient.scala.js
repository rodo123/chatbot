var chatlogIni =
    '<div>' + 
        '<table>' +
            '<thead>' +
                '<tr>' + 
                    '<th>FECHA</th>' +
                    '<th>NOMBRE</th>' +
                    '<th>TEXTO</th>' +
                '</tr>' +
            '</thead>' +
            '<tbody id="uiTableLogChatBody">';
var chatlogFin = 
            '</tbody>' +
        '</table>' +
    '</div>';

var compName = "Infobox";


(function($) {
    $.widget("ui.clientChatbox", {
        options: {
            id: null,
            title: null,
            user: null,
            hidden: false,
            webSocket: null,
            width: 300, 
            currentMsg: "",
            fechaIni: -1,
            timerEnvioId: 0,
            closed: false,
            timerRespuestaId: 0,
            messageSent: function(id, user, msg) {
                
                this.webSocket.send(JSON.stringify({
                    text: msg
                }))
            },
            notifyTyping: function (isTyping) {
                this.webSocket.send(JSON.stringify({
                    typing: isTyping
                }))
            },
            guardarSesion: function(id) {
                var json = {
                    sesion: id
                };

                var elem = this;
                var currentLog = chatlogIni;

                $("#uiTableLogChatBody" + id).html("");

                $.ajax({
                    /*type: "POST" ,
 *                     url: BASE_URL+"/chat/chat/getChatLines" ,
 *                                         data: JSON.stringify( json ) ,
 *                                                             dataType: "json" ,
 *                                                                                 contentType: "application/json" ,*/
                    url: BASE_URL+"/chat/chat/getChatLines" ,
                    type: "GET",
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    data: {sesion:getCookie("ID_SESSION")} ,
                    success: function( data ){
                        var line;
                        console.log("data <" + data + ">");
                        var results = "";
                        for( var i = 0 ; i < data.length ; i++){
                            line = data[i];

                            results += "<tr>" + "<td>(" + getHHMMSS(line.timestamp) + ")</td>" + "<td>" + line.publicname + "</td>" + "<td>" + line.text + "</td>" + "</tr>"; 

                            
                            $("#uiTableLogChatBody" + id)
                                .append($("<tr>")
                                    .append( $( "<td>" ).text( line.timestamp ) )
                                    .append( $( "<td>" ).text( line.publicname ) )
                                    .append( $( "<td>" ).text( line.text ) )
                                );
                            
                        }

                        elem.guardar(compName + (elem.fechaIni > 0 ? (" - " + new Date(elem.fechaIni)) : "") + "\n\n" + chatlogIni + results + chatlogFin);
                    } ,
                    error: function( data ){
                        console.log( data ) ;
                        elem.guardar(compName + (elem.fechaIni > 0 ? (" - " + new Date(elem.fechaIni)) : "") + "\n\n" + chatlogIni + results + chatlogFin);
                    }
                } ) ;
            },
            guardar: function(text) {
                var pdf = new jsPDF('p', 'pt', 'letter')
                , source = text
                , specialElementHandlers = {
                    
                    '#bypassme': function(element, renderer){
                        
                        return true
                    }
                }
                margins = {
                  top: 80,
                  bottom: 60,
                  left: 40,
                  width: 522
                };
                pdf.fromHTML(
                    source 
                    , margins.left 
                    , margins.top 
                    , {
                        'width': margins.width 
                        , 'elementHandlers': specialElementHandlers
                    },
                    function (dispose) {
                      
                      pdf.save('chatlog.pdf');
                    },
                    margins
                )
            },
            boxClosed: function(id) {
                this.notifyTyping(false);
                this.webSocket.onclose = function () {};
                this.webSocket.close();
            },
            boxManager: {
                
                init: function(elem) {
                    this.elem = elem;
                    console.log("id: <" + this.elem.options.id + ">");
                    var json = {
                        sesion: this.elem.options.id
                    };



                   
                    $.ajax({
                        url: BASE_URL+"/chat/chat/getChatLines" ,
                        type: "GET",
                        xhrFields: {
                            withCredentials: true
                        },
                        crossDomain: true,
                        data: {sesion:getCookie("ID_SESSION")} ,
                        success: function( data ){
                            var line;
                            console.log("data <" + data + ">");
                            for( var i = 0 ; i < data.length ; i++){
                                line = data[i];
                                elem.options.boxManager.addMsg(line);
                            }
                        } ,
                        error: function( data ){
                            console.log( data ) ;
                        }
                    } ) ;
                    
                },
                addMsg: function(line) {
                    var time = line.timestamp;
                    var peer = line.publicname;
                    var isClient = line.isClient;
                    var msg = line.text;
                    var tipo = line.tipo;
                    var self = this;
                    var box = self.elem.uiChatboxLog;
                    var e = document.createElement('div');
                    box.append(e);
                    $(e).hide();

                    var systemMessage = false;
                    var tiempo = "(" + getHHMMSS(time) + ")";

                    switch(tipo) {
                        case 999:   systemMessage = true;
                                    break;

                        case -1:    systemMessage = true;
                                    self.elem.options.closed = true;
                                    $(self.elem.uiChatboxInputBox).prop('disabled', true);
                                    break;

                        case 0:     
                        case 3:     var peerName = document.createElement("b");
                                    $(peerName).text(tiempo + " " + peer + ": ");
                                    e.appendChild(peerName);
                                    break;

                        case 7:     if (line.typing)
                                        $("#divChatboxAlert"+self.elem.options.id).html(peer + ' está escribiendo');
                                    else {
                                        $("#divChatboxAlert"+self.elem.options.id).html("");
                                        if (self.elem.options.timerRespuestaId) {
                                            clearTimeout(self.elem.options.timerRespuestaId);
                                            self.elem.options.timerRespuestaId = 0;
                                        }
                                        if (!self.elem.options.closed) {
                                            $(self.elem.uiChatboxInputBox).prop('disabled', false);
                                            self.elem.uiChatboxInputBox.focus();
                                        }
                                    }
                                    return;

                        default:    return;
                    }


                    var msgElement = document.createElement(
                        systemMessage ? "i" : "span");
                    $(msgElement).text(systemMessage ? tiempo + " " + msg : msg);
                    e.appendChild(msgElement);
                    $(e).addClass("ui-chatbox-msg");
                    $(e).css("maxWidth", '340px'/*$(box).width()*/);

                    if (isClient) {
                        $(e).addClass("externo");
                    } else {
                        $(e).addClass("interno");
                    }

                    $(e).fadeIn();
                    self._scrollToBottom();

                    if (!self.elem.uiChatboxTitlebar.hasClass("ui-state-focus")
                        && !self.highlightLock) {
                        self.highlightLock = true;
                        self.highlightBox();
                    }
                },
                highlightBox: function() {
                    var self = this;
                    self._scrollToBottom();
 
                },
                toggleBox: function() {
                    this.elem.uiChatbox.toggle();
                },
                _scrollToBottom: function() {
                    var box = this.elem.uiChatboxLog;
                    box.scrollTop(box.get(0).scrollHeight);
                }
            }
        },
        toggleContent: function(event) {
            this.uiChatboxContent.toggle();
            if (this.uiChatboxContent.is(":visible")) {
                this.uiChatboxInputBox.focus();
            }
        },
        widget: function() {
            return this.uiChatbox
        },
        _create: function() {
            var self = this,
            options = self.options,
            title = options.title || '',

            uiChatbox = (self.uiChatbox = $('<div id="uiChatbox'+self.options.id+'"></div>'))
                .show(function(event) {
                   $("#uiChatbox"+self.options.id).draggable({
                        containment: 'window',
                        scroll: false,
                        handle: '#uiChatboxTitlebar'+self.options.id
                    });
                })
                .appendTo(document.body)
                .addClass('ui-widget ' +
                          'ui-widget-content ' +
                          'ui-corner-top ' +
                          'ui-chatbox ' +
                          'ui-chatbox-force-top'
                         )
                .focusin(function() {
                    self.uiChatboxTitlebar.addClass('ui-state-focus');
                })
                .focusout(function() {
                    self.uiChatboxTitlebar.removeClass('ui-state-focus');
                }),

                uiChatboxTitlebar = (self.uiChatboxTitlebar = $('<div id="uiChatboxTitlebar'+self.options.id+'"></div>'))
                    .addClass('ui-widget-header ' +
                              'ui-corner-top ' +
                              'ui-chatbox-titlebar ' +
                              'ui-dialog-header'
                             )
                    .click(function(event) {
                    })
                    .appendTo(uiChatbox),

                    uiChatboxTitle = (self.uiChatboxTitle = $('<span></span>'))
                        .html(title)
                        .appendTo(uiChatboxTitlebar),

                    uiChatboxTitlebarClose = (self.uiChatboxTitlebarClose = $('<a href="#"></a>'))
                        .addClass('ui-corner-all ' +
                                  'ui-chatbox-icon '
                                 )
                        .attr('role', 'button')
                        .hover(function() { uiChatboxTitlebarClose.addClass('ui-state-hover'); },
                               function() { uiChatboxTitlebarClose.removeClass('ui-state-hover'); })
                        .click(function(event) {
                            if(!confirm('¿Desea cerrar la conversación actual?')) return;
                            uiChatbox.hide();
                            self.options.boxClosed(self.options.id);
                            self.uiChatbox.remove();
                            self.options.mainManager.delBox(self.options.id);
                            delCookie("SESSION_CHAT");
                            delCookie("ID_SESSION");
			    delCookie("ID_DIAGRAMA");
                            $.ajax({
                              url: BASE_URL+"/logout",
                              type: "GET",
                              xhrFields: {
                                withCredentials: true
                              },
                              crossDomain: true
                            }).done( function(res){


                            }).fail(function(jqXHR, textStatus, errorThrown){
                     
                                console.log('Hubo un error al levantar el chat: '+errorThrown);
                                
                            });
                            return false;
                        })
                        .appendTo(uiChatboxTitlebar),

                        uiChatboxTitlebarCloseText = $('<span></span>')
                            .addClass('ui-icon ' +
                                      'ui-icon-closethick ' +
                                      'ui-button-bg-images')
                            .text('CERRAR')
                            .appendTo(uiChatboxTitlebarClose),

                    uiChatboxTitlebarMinimize = (self.uiChatboxTitlebarMinimize = $('<a href="#"></a>'))
                        .addClass('ui-corner-all ' +
                                  'ui-chatbox-icon'
                                 )
                        .attr('role', 'button')
                        .hover(function() { uiChatboxTitlebarMinimize.addClass('ui-state-hover'); },
                               function() { uiChatboxTitlebarMinimize.removeClass('ui-state-hover'); })
                        .click(function(event) {
                            self.toggleContent(event);
                            return false;
                        })
                        .appendTo(uiChatboxTitlebar),

                        uiChatboxTitlebarMinimizeText = $('<span></span>')
                            .addClass('ui-icon ' +
                                      'ui-icon-minusthick ' +
                                      'ui-button-bg-images')
                            .text('MINIMIZAR')
                            .appendTo(uiChatboxTitlebarMinimize),

                uiChatboxContent = (self.uiChatboxContent = $('<div></div>'))
                    .addClass('ui-widget-content ' +
                              'ui-chatbox-content '
                             )
                    .appendTo(uiChatbox),

                    uiDivGuardar = (self.uiDivGuardar = $('<div id="divGuardar'+self.options.id+'"></div>'))
                                .show(function(event) {
                                   $("#divGuardar"+self.options.id).buttonset();
                                })
                                .appendTo(uiChatboxContent)
                                .addClass('ui-widget-content ' +
                                          'ui-chatbox-options-borderless'),

                                uiLabelGuardar = (self.uiLabelGuardar = $('<label id="uiLabelGuardar'+self.options.id+'">GUARDAR CONVERSACIÓN</label>'))
                                    .show(function(event) {
                                       $("#uiLabelGuardar"+self.options.id).button();
                                    })
                                    .click(function(event) {
                                        self.options.guardarSesion(self.options.id);
                                    })
                                    .appendTo(uiDivGuardar)
                                    .addClass(
                                              'ui-chatbox-button-guardar'
                                             ),

                    uiChatboxLog = (self.uiChatboxLog = self.element)
                        .addClass('ui-chatbox-log ' +
                                  'ui-widget-content'
                                 )
                        .appendTo(uiChatboxContent),

                    uiChatboxAlert = (self.uiChatboxAlert = $('<div id="divChatboxAlert'+self.options.id+'"></div>'))
                        .addClass('ui-chatbox-alert')
                        .appendTo(uiChatboxContent),

                    uiChatboxInput = (self.uiChatboxInput = $('<div></div>'))
                        .addClass('ui-chatbox-input'
                                 )
                        .click(function(event) {

                        })
                        .appendTo(uiChatboxContent),

                        uiChatboxInputBox = (self.uiChatboxInputBox = $('<textarea></textarea>'))
                            .addClass('ui-widget-content ' +
                                      'ui-chatbox-input-box ' +
                                      'ui-corner-all'
                                     )
                            .appendTo(uiChatboxInput)
                            .keydown(function(event) {
                                if (event.keyCode && event.keyCode == $.ui.keyCode.ENTER) {
                                    msg = $.trim($(uiChatboxInputBox).val());
                                    if (msg.length > 0) {
                                        $("#divChatboxAlert"+self.options.id).html("Enviando...");
                                        if (self.options.currentMsg.length > 0)
                                            self.options.currentMsg += "\n" + msg;
                                        else
                                            self.options.currentMsg = msg;
                                        if (self.options.timerEnvioId) {
                                            clearTimeout(self.options.timerEnvioId);
                                        }
                                        self.options.timerEnvioId = setTimeout(function () {
                                            $(uiChatboxInputBox).prop('disabled', true);
                                            self.options.timerEnvioId = 0;
                                            self.options.timerRespuestaId = setTimeout(function () {
                                                $(uiChatboxInputBox).prop('disabled', false);
                                                self.uiChatboxInputBox.focus();
                                            }, 90000);
                                            self.options.messageSent(self.options.id, self.options.user, self.options.currentMsg);
                                            self.options.currentMsg = "";
                                            $("#divChatboxAlert"+self.options.id).html("");
                                        }, 2000);
                                    }
                                    $(uiChatboxInputBox).val('');
                                    return false;
                                }
                            })
                            .keyup(function(event) {
                                msg = $.trim($(this).val());
                                if (msg.length > 0) {
                                    self.options.notifyTyping(true);
                                } else {
                                    self.options.notifyTyping(false);
                                }
                                return true;
                            })
                            .focusin(function() {
                                uiChatboxInputBox.addClass('ui-chatbox-input-focus');
                                var box = $(this).parent().prev();
                                box.scrollTop(box.get(0).scrollHeight);
                            })
                            .focusout(function() {
                                uiChatboxInputBox.removeClass('ui-chatbox-input-focus');
                            });

            uiChatboxTitlebar.find('*').add(uiChatboxTitlebar).disableSelection();

            uiChatboxContent.children().click(function() {
                self.uiChatboxInputBox.focus();
            });

            self._setWidth(self.options.width);
            self._position(self.options.offset);

            self.options.boxManager.init(self);

            if (!self.options.hidden) {
                uiChatbox.show();
            }
        },
        _setOption: function(option, value) {
            if (value != null) {
                switch (option) {
                case "hidden":
                    if (value)
                        this.uiChatbox.hide();
                    else
                        this.uiChatbox.show();
                    break;
                case "offset":
                    this._position(value);
                    break;
                case "width":
                    this._setWidth(value);
                    break;
                }
            }
            $.Widget.prototype._setOption.apply(this, arguments);
        },
        _setWidth: function(width) {
            this.uiChatboxTitlebar.width(width + "px");
            this.uiChatboxLog.width(width + "px");
            this.uiChatboxInput.css("maxWidth", width + "px");
        },
        _position: function(offset) {

        }
    });
}(jQuery));
